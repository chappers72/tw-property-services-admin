# tw-property-services-admin

## Project setup

##Running
1. Type vue ui in the terminal which will start the vue client on port 8000 locally.
2. Under Tasks select serve.
3. Browse to http://localhost:8080/#/

```
##Deploying to Digital Ocean
1. Type npm run build into the terminal
2. Files get generated into the dist folder
3. FTP files to Digital Ocean to var/www/tw-property-services-ui/public/js/
