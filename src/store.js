import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

const apiClient = axios.create({
    baseURL: `${process.env.VUE_APP_API_SERVER}/api`,
    withCredentials: false,
    headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
    }
});
Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        status: '',
        token: localStorage.getItem('token') || '',
        user: {}
    },
    mutations: {
        auth_request(state) {
            state.status = 'loading'
        },
        auth_success(state, token, user) {
            state.status = 'success';
            state.token = token;
            state.user = user;
        },
        auth_error(state) {
            state.status = 'error'
        },
        logout(state) {
            state.status = '';
            state.token = '';
        },
    },
    actions: {
        logout({commit}) {
            return new Promise((resolve, reject) => {
                commit('logout');
                localStorage.removeItem('token');
                delete axios.defaults.headers.common['Authorization'];
                resolve()
            })
        },
        async register({commit}, _user) {
            let result = await apiClient.post("/user", _user);
            if (result.status === 200) {
                const token = result.data.token;
                const user = result.data.user;
                localStorage.setItem('token', token);
                axios.defaults.headers.common['Authorization'] = token;
                commit('auth_success', token, user);
                return result;
            } else {
                commit('auth_error')
                localStorage.removeItem('token')
                return result;
            }
        },
        async login({commit}, _user) {
            let result = await apiClient.post("/user/login", _user);
            if (result.status === 200) {
                const token = result.data.token;
                const user = result.data.user;
                localStorage.setItem('token', token);
                axios.defaults.headers.common['Authorization'] = token;
                commit('auth_success', token, user);
                return result;
            } else {
                commit('auth_error');
                localStorage.removeItem('token');
                return result;
            }
        },
    },
    getters: {
        isLoggedIn: state => !!state.token,
        authStatus: state => state.status,
        currUserName: state => parseJwt(state.token),
    }
})
function parseJwt (token) {
    var base64Url = token.split('.')[1];
    var base64 = base64Url.replace('-', '+').replace('_', '/');
    return JSON.parse(window.atob(base64));
};
