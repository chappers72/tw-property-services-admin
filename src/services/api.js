import axios from "axios";

const apiClient = axios.create({
    baseURL: `${process.env.VUE_APP_API_SERVER}/api`,
    withCredentials: false,
    headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
    }
});

export default {
    async getUsers() {
        let result = await apiClient.get("/users");
        return result.data;
    },
    async getUser(_id) {
        let result = await apiClient.get(`/user/${_id}`);
        return result.data;
    },
    async saveUser(_data) {
        if(_data._id){
            let result = await apiClient.put(`/user/${_data._id}`,_data);
            return result;
        }else{
            let result = await apiClient.post(`/user`,_data);
            return result;
        }
    },
    async sendPasswordLink(_data){
        let result = await apiClient.post(`/user/forgotten`,_data);
        return result.data;
    },
    async deleteUser(_id){
        let result = await apiClient.delete(`/user/${_id}`);
        return result.data;
    },
    async getTestimonials() {
        let result = await apiClient.get("/testimonials");
        return result.data;
    },
    async getTestimonial(_id) {
        let result = await apiClient.get(`/testimonial/${_id}`);
        return result.data;
    },
    async saveTestimonial(_data) {
        if(_data._id){
            let result = await apiClient.put(`/testimonial/${_data._id}`,_data);
            return result;
        }else{
            let result = await apiClient.post(`/testimonial`,_data);
            return result;
        }
    },
    async deleteTestimonial(_id){
        let result = await apiClient.delete(`/testimonial/${_id}`);
        return result.data;
    },
    async getSIPs() {
        let result = await apiClient.get("/sips");
        return result.data;
    },
    async getSIP(_id) {
        let result = await apiClient.get(`/sip/${_id}`);
        return result.data;
    },
    async saveSIP(_data) {
        if(_data._id){
            let result = await apiClient.put(`/sip/${_data._id}`,_data);
            return result;
        }else{
            let result = await apiClient.post(`/sip`,_data);
            return result;
        }
    },
    async deleteSIP(_id){
        let result = await apiClient.delete(`/sip/${_id}`);
        return result.data;
    },
    async getBlogs() {
        let result = await apiClient.get("/blogs");
        return result.data;
    },
    async getBlog(_id) {
        let result = await apiClient.get(`/blog/${_id}`);
        return result.data;
    },
    async saveBlog(_data) {
        if(_data._id){
            let result = await apiClient.put(`/blog/${_data._id}`,_data);
            return result;
        }else{
            let result = await apiClient.post(`/blog`,_data);
            return result;
        }
    },
    async deleteBlog(_id){
        let result = await apiClient.delete(`/blog/${_id}`);
        return result.data;
    },
    async getProjects() {
        let result = await apiClient.get("/projects");
        return result.data;
    },
    async getProject(_id) {
        let result = await apiClient.get(`/project/${_id}`);
        return result.data;
    },
    async saveProject(_data) {
        if(_data._id){
            let result = await apiClient.put(`/project/${_data._id}`,_data);
            return result;
        }else{
            let result = await apiClient.post(`/project`,_data);
            return result;
        }
    },
    async deleteProject(_id){
        let result = await apiClient.delete(`/project/${_id}`);
        return result.data;
    }, async getGalleries() {
        let result = await apiClient.get("/galleries");
        return result.data;
    },
    async getGallery(_id) {
        let result = await apiClient.get(`/gallery/${_id}`);
        return result.data;
    },
    async saveGallery(_data) {
        if(_data._id){
            let result = await apiClient.put(`/gallery/${_data._id}`,_data);
            return result;
        }else{
            let result = await apiClient.post(`/gallery`,_data);
            return result;
        }
    },
    async deleteGallery(_id){
        let result = await apiClient.delete(`/gallery/${_id}`);
        return result.data;
    },
    search(query) {
        return apiClient.post("/search", query);
    }
};
