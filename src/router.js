import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Users from './views/Users.vue'
import User from './views/User.vue'
import Login from './views/Login.vue'
import Testimonials from './views/Testimonials.vue'
import Testimonial from './views/Testimonial.vue'
import SIPs from './views/SIPs.vue'
import SIP from './views/SIP.vue'
import Blogs from './views/Blogs.vue'
import Blog from './views/Blog.vue'
import Galleries from './views/Galleries.vue'
import Gallery from './views/Gallery.vue'
import Projects from './views/Projects.vue'
import Project from './views/Project.vue'
import store from './store.js'

const router = new Router({
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/login',
            name: 'login',
            component: Login
        },
        {
            path: '/users',
            name: 'users',
            component: Users,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/user',
            name: 'user',
            component: User,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/testimonials',
            name: 'testimonials',
            component: Testimonials,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/testimonial/:id',
            name: 'testimonial',
            component: Testimonial,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/blogs',
            name: 'blogs',
            component: Blogs,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/blog/:id',
            name: 'blog',
            component: Blog,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/projects',
            name: 'projects',
            component: Projects,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/project/:id',
            name: 'project',
            component: Project,
            meta: {
                requiresAuth: true
            }
        },    {
            path: '/galleries',
            name: 'galleries',
            component: Galleries,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/gallery/:id',
            name: 'gallery',
            component: Gallery,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/sips',
            name: 'sips',
            component: SIPs,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/sip/:id',
            name: 'sip',
            component: SIP,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/about',
            name: 'about',
            // route level code-splitting
            // this generates a separate chunk (about.[hash].js) for this route
            // which is lazy-loaded when the route is visited.
            component: () => import(/* webpackChunkName: "about" */ './views/About.vue'),
            meta: {
                requiresAuth: true
            }
        }
    ]
});
Vue.use(Router);

router.beforeEach((to, from, next) => {
    if(to.matched.some(record => record.meta.requiresAuth)) {
        if (store.getters.isLoggedIn) {
            next();
            return
        }
        next('/login')
    } else {
        next()
    }
});

export default router
