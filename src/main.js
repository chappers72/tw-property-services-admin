import Vue from 'vue'
import App from './App.vue'
import router from './router'
import Vuetify from 'vuetify'
import store from '@/store.js'

// index.js or main.js
import 'vuetify/dist/vuetify.min.css' // Ensure you are using css-loader
import VeeValidate from 'vee-validate'
import CKEditor from '@ckeditor/ckeditor5-vue';

Vue.use( CKEditor );
Vue.use(Vuetify);
Vue.use(VeeValidate);
Vue.config.productionTip = false;

new Vue({
  router,
    store,
  render: h => h(App)
}).$mount('#app')
